#########
Übersicht
#########

Durch das ISMS-Add-on wird eine neue Objekttypgruppe "ISMS" erstellt. Dieser Objekttypgruppe werden zehn neue Objekttypen zugeordnet:

* `Audit <objekttypen.html#objekttyp-audit>`_
* `Bedrohung <objekttypen.html#objekttyp-bedrohung>`_
* `Bewertungskriterium <objekttypen.html#objekttyp-bewertungskriterium>`_
* `Ereignis <objekttypen.html#objekttyp-ereignis>`_
* `Maßnahme Anhang A <objekttypen.html#objekttyp-masznahme-anhang-a>`_
* `Normanforderung <objekttypen.html#objekttyp-normanforderung>`_
* `Schadensszenario <objekttypen.html#objekttyp-schadensszenario>`_
* `Schwachstelle <objekttypen.html#objekttyp-schwachstelle>`_
* `SOA-Maßnahme <objekttypen.html#objekttyp-soa-masznahme>`_
* `Virtueller ISMS-Standort <objekttypen.html#objekttyp-virtueller-isms-standort>`_


Für mehrere Objekttypen stehen dabei mögliche CSV-Import Dateien bereit. Die Objekte und Objekttypen sind dabei jedoch wie in i-doit gewohnt leicht an die eigenen Bedürfnisse anzupassen. So unterstützt das ISMS z.B. bei der Berücksichtigung der :doc:`standortrechte`.

Das ISMS-Add-on installiert mehrere :doc:`reports` zur erweiterten Auswertung und Dokumentation des ISMS.