##############
Standortrechte
##############

Um das Konzept der Rechtevergabe nach Standorten zu unterstützen, ist es möglich, in den Mandanteneinstellungen von i-doit die Option "Reportviews nach Standortrechten filtern" zu aktivieren.
Dies bewirkt, dass in den vom ISMS-Add-on ausgelieferten Report Views nur die Risikoeinschätzungen einbezogen werden, die der Nutzer einsehen darf. Genauer heißt das, es wird geprüft, ob der Nutzer an den jeweiligen Objekten für die Kategorie "ISMS" das Recht "Ansehen" hat.

Es ist weiterhin möglich, die Kategorie "Standort" an Objekte vom Typ "SOA-Maßnahme" oder "Maßnahme Anhang A" zu konfigurieren, um auch die Maßnahmen entsprechend Standorten zuweisen zu können.
So wird es möglich, dass die Standorte selbst den Umsetzungstatus ihrer eigenen Maßnahmen pflegen können, ohne den Status der Maßnahmen anderer Standorte einsehen zu können. Eine zentrale Stelle hingegen könnte eine Auswertung über alle Standorte ausgeben lassen (bzw. durch den Standortfilter auch jeweils einzelne Standorte).