############
Installation
############

Die Installation des ISMS Add-ons entspricht dem Standardvorgehen für die Installation von i-doit Add-ons:

* Einloggen in des i-doit Admin-Center
* Auf den Reiter "Add-ons" gehen
* Auf den Button "Install/update Add-on" klicken
* Das ZIP-Paket des Add-ons auswählen
* Auf den Knopf "Upload und install" klicken
* Fertig

Einzige Besonderheit: Das Add-on trägt in der Liste den Namen ISO27001.

Nach der Installation muss das ISMS-Add-on noch konfiguriert werden. Informationen dazu finden Sie unter :doc:`einrichtung`.