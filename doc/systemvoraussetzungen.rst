#####################
Systemvoraussetzungen
#####################

Als Add-on für die i-doit CMDB setzt das ISMS-Add-on natürlich ein funktionsfähiges i-doit voraus.

Die aktuelle Version des ISMS-Add-on setzt die i-doit Version 1.14.2 voraus.

..  warning::
    Die Version 1.5.4 des ISMS-Add-ons ist kompatibel mit i-doit Versionen <= 26, die Version 1.5.5 ist nur kompatibel für i-doit Versionen >= 27.

..  note::
    Diese Dokumentation bezieht sich -wenn nicht anders erwähnt- immer auf die aktuelle Version des ISMS-Add-on.

