######################################################
Willkommen zur Dokumentations des i-doit Add-ons ISMS
######################################################

Das Add-on ISMS dient dazu, das Risiko- und Maßnahmenmanagement direkt an den jeweiligen Objekten der IT-Dokumentation zu bewerten, zu dokumentieren und zu analysieren sowie darüber hinaus vorzeigbare Auswertungen mit Hilfe des Report Managers zu generieren. Es unterstützt hierbei die Dokumentationsprozesse der ISO-27000-Norm-Familie.

------------

.. toctree::
    :maxdepth: 1
    :caption: Erste Schritte
    
    einfuehrung
    systemvoraussetzungen
    installation
    einrichtung


.. toctree::
    :maxdepth: 1
    :caption: Funktionsübersicht
    
    uebersicht
    objekttypen
    reports
    risikoeinschaetzung
    import
    standortrechte

.. toctree::
    :maxdepth: 1
    :caption: Historie
    
    changelog 


##################
License
##################

`becon`_ © 2013-2020 becon GmbH

.. _becon: LICENSE.html