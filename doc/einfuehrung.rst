##########
Einführung
##########

Das Add-on ISMS dient dazu, das Risiko- und Maßnahmenmanagement direkt an den jeweiligen Objekten der IT-Dokumentation zu bewerten, zu dokumentieren und zu analysieren sowie darüber hinaus vorzeigbare Auswertungen mit Hilfe des Report Managers zu generieren. Es unterstützt hierbei die Dokumentationsprozesse der ISO-27000-Norm-Familie.

* Unterstützung und Dokumentation von ISO27001 Zertifizierung
* Risikoeinschätzungen und Maßnahmen für ISO 270xx, KRITIS oder BSI Standard 200-3 dokumentieren und verwalten
* Risikomanagement auf allen Ebenen des Informationsverbundes 
* Import und Verwaltung von Bedrohungen, Maßnahmen und Schwachstellen aus externen Katalogen
* Umfangreiche Reports, wie z.B. Risikomatrix, Erklärung zur Anwendbarkeit (SOA), Risikobehandlungsplan (RTP), usw.

